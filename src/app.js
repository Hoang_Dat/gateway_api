const express = require("express");
const app = express();

const cors = require("cors");
const routes = require("./routes");
const mongoose = require("mongoose");
const boolParser = require('express-query-boolean');
const accountDB = require('./config/database');
const imageDB = require('./config/image_DB');
const chattingServices_GRPC = require("./config/chattingServices_GRPC");


app.use(cors());
app.use(boolParser());
app.use(express.json({ limit: "6mb", extended: true }));
app.use(express.urlencoded({ limit: "6mb", extended: true }));

app.get("/", (req, res) => {
  res.send("Wellcome To English Communication System!");
});

app.use("/api", routes);
const server = require('http').createServer(app);
const socketio = require('socket.io')(server, {
  pingTimeout: 5000,
});
const sockets = {};
socketio.on('connection', (socket) => {
  socket.on('initRoom', (userId) => {
    sockets[userId.senderId] = socket;
    console.log(userId)
  });
  socket.on('message', (message) => {
    console.log("dsiudhsuhdsuhd", message)
    const ChatMessageRequest = {
      roomId: message.roomId,
      message: message.message,
      createdBy: message.senderId,
    }
    chattingServices_GRPC.InsertMessageChat(ChatMessageRequest, (error, ChatMessage) => {
      if (error) {
        console.log(error);
      }
    })
    if (sockets[message.receiverId]) {
      sockets[message.receiverId].emit('message', message);
    }
  });
  socket.on('disconnect', (userId) => {
    delete sockets[userId.senderId];
  });
});
module.exports = server;