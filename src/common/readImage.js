const  mongoose = require('mongoose');
const Grid = require('gridfs-stream');
const image_DB = require("../config/image_DB");
const appSettings = require("../propertys");
class readImage{
    constructor(){   
        image_DB.once('open', function () {
            console.log("image database read succes.");
            this.gfs = Grid(image_DB.db, mongoose.mongo);
            this.gfs.collection(appSettings.UPLOAD_COLLECTION);
          }).on('error', function (error) {
            console.log("image database read false: " + error);
          })
    }
    readimage(){
      return this.gfs;
    }
}
module.exports = new readImage()