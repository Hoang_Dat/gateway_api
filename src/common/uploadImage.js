const path = require('path');
const crypto = require('crypto');
const multer = require('multer');
const image_DB = require("../config/image_DB")
const GridFsStorage = require('multer-gridfs-storage');
const appSettings = require('../propertys');

const storage = new GridFsStorage({
  url: 'mongodb://13.67.90.77:27017/imageUpload',
  file: (req, file) => {
    console.log("req", req)
    return new Promise((resolve, reject) => {
      crypto.randomBytes(16, (err, buf) => {
        if (err) {
          return reject(err);
        }
        console.log("file", file)
        const filename = buf.toString('hex') + path.extname(file.originalname);
        const fileInfo = {
          filename: filename,
          bucketName: 'upload'
        };
        resolve(fileInfo);
      });
    });
  }
});
const upload = multer({ storage });
module.exports = upload