const crypto = require("crypto");
const DEFAULT_LENGHT = 24;
const EC = require('elliptic').ec;
const ec = new EC('secp256k1');

const getHashString = (sourceString, saltString) => {
  return crypto
    .createHash("sha256")
    .update(sourceString + saltString)
    .digest("hex")
    .toString();
};

const getRandomString = (length = DEFAULT_LENGHT) => {
  return crypto.randomBytes(length).toString("hex");
};

const getPrivateKey = () => {
   const accessKey = ec.genKeyPair();
   return accessKey.getPrivate('hex');
}

module.exports = { getHashString, getRandomString, getPrivateKey };