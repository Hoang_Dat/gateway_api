const Schema = require("mongoose").Schema;

const userInfoSchema = new Schema({
  username: { type: String, required: true },
  email: { type: String },
  displayName: { type: String },
  address: { type: String },
  gender:Boolean,
  avatar:{ type: String, default: "8bed048f77f2dda1478c78c42ff296c8.png" },
  phoneNumber:{ type: String },
  favourite:{type:Array},
  createdDay: { type: Date, default: Date.now }
});

module.exports = userInfoSchema;