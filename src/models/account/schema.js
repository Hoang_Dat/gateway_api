const Schema = require("mongoose").Schema;

const accountSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  privateKey: {
    type: String,
    required: true
  },
  status: { type: Boolean, default: true },
  role: { type: String, default: "client" },
  createdDay: { type: Date, default: Date.now }
});

module.exports = accountSchema;