const jwt = require("jsonwebtoken");
const appSettings = require("../propertys");
const {
  Forbidden,
  InternalServerError,
  Unauthorized
} = require("../helpers/responseHelper");
const { getUserRoleById, getAccountById } = require("../services/accountServices");
const LockedUser = "you are blocked, please contact admin for more detail!";

const requiredLogin = (req, res, next) => {
  try {
    let token = req.headers["x-access-token"] || req.headers["authorization"];
    if (token && token.startsWith("Bearer ")) {
      token = token.split(" ")[1];
    }
    if (token) {
      jwt.verify(token, appSettings.SECRETKEY, async (err, decoded) => {
        if (err) {
          return Unauthorized(res, "Invalid Token!");
        }
        req.decoded = decoded;
        const {status} = await getAccountById(decoded.id);
        if (!status) return Unauthorized(res, LockedUser);
        next();
      });
    } else Forbidden(res, "Not found Token !");
  } catch (e) {
    console.log("REQUIRED LOGIN ERROR", e);
    InternalServerError(res);
  }
};

const requiredAdmin = async (req, res, next) => {
  requiredLogin(req, res, async () => {
    try {
      const { role } = await getUserRoleById(req.decoded.id);
      if (role.includes("admin")) {
        next();
      } else {
        return Forbidden(res, "This action requires admin role!");
      }
    } catch (e) {
      console.log("REQUIRED ADMIN ERROR", e);
      InternalServerError(res);
    }
  });
};

module.exports = { requiredLogin, requiredAdmin };