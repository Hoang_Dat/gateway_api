const PROTO_PATH = __dirname+ '/protos/blockchain.proto';
const grpc = require('grpc');
var protoLoader = require('@grpc/proto-loader');
var packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });
const blockchain_proto = grpc.loadPackageDefinition(packageDefinition);
const transaction_GRPC = new blockchain_proto.TransactionService('127.0.0.1:50053',
    grpc.credentials.createInsecure());

module.exports = transaction_GRPC