const PROTO_PATH = __dirname + '/protos/chatting.proto';
const grpc = require('grpc');
var protoLoader = require('@grpc/proto-loader');
var packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });
const chatting_proto = grpc.loadPackageDefinition(packageDefinition);
const chatting_GRPC = new chatting_proto.ChattingService('127.0.0.1:50059',
    grpc.credentials.createInsecure());
module.exports = chatting_GRPC