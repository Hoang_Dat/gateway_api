const PROTO_PATH = __dirname+ '/protos/information.proto';
const grpc = require('grpc');
var protoLoader = require('@grpc/proto-loader');
var packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    });
const information_proto = grpc.loadPackageDefinition(packageDefinition);
const information_GRPC = new information_proto.InformationService('127.0.0.1:50057',
    grpc.credentials.createInsecure());

module.exports = information_GRPC