const mongoose = require('mongoose');
const appSettings = require('../propertys');
const image_connection = mongoose.createConnection(appSettings.IMAGE_MONGODB_URL,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
});

module.exports = image_connection