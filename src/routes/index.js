const routes = require("express")();

routes.use("/admin/accounts", require("../controllers/account").adminRouter);
routes.use("/accounts", require("../controllers/account").router);

routes.use("/", require("../controllers/authentication"));
routes.use("/transactions", require("../controllers/transaction").router);
routes.use("/topics", require("../controllers/topic").router);
routes.use("/places", require("../controllers/place").router);
routes.use("/appointments", require("../controllers/appointment").router);
routes.use("/images", require("../controllers/image").router);
routes.use("/chattings", require("../controllers/chatting").router);
module.exports = routes;