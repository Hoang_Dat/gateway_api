const jwt = require("jsonwebtoken");
const appSettings = require("../../propertys");
const { getHashString } = require("../../helpers/hashHelper");
const {
  BadRequest,
  InternalServerError,
  Unauthorized
} = require("../../helpers/responseHelper");
const { getAccountByUsername } = require("../../services/accountServices");

const DefNoRememberTime = appSettings.TOKEN_EXPIRE_TIMESHORT;
const DefRememberTime = appSettings.TOKEN_EXPIRE_TIMELONG;
const WrongAccountMsg = "Wrong username or password";
const LockedUser = "you are blocked, please contact admin for more detail!";

const login = async (req, res) => {
  let { username, password, remember } = req.body;
  if (!username || !password) return BadRequest(res);
  username = username.trim();
  try {
    const account = await getAccountByUsername(username);
    if (account && isMatchPassword(account, password)) {
      if(!account.status) return Unauthorized(res, LockedUser);
      responseUserSession(res, account, remember);
    } else {
      Unauthorized(res, WrongAccountMsg);
    }
  } catch (err) {
    console.log(err)
    InternalServerError(res);
  }
};

const createToken = (user, expireTime = DefNoRememberTime) => {
  return jwt.sign(
    { username: user.username, id: user._id, role : user.role },
    appSettings.SECRETKEY,
    {
      expiresIn: expireTime
    }
  );
};

const isMatchPassword = (user, password) => {
  const hashPassword = getHashString(password, user.privateKey);
  if (hashPassword === user.password) {
    return true;
  }
  return false;
};

const responseUserSession = (res, user, remember) => {
  const { username , _id :id, role} = user;
  const expireTime = remember ? DefRememberTime : DefNoRememberTime;
  res.json({ token: createToken(user, expireTime), username, id, role});
};

module.exports = login;
