const {
    insertAccount,
    getAccountByUsername
  } = require("../../services/accountServices");
  const { insertUserInfo } = require("../../services/userInforServices");
  const {
    BadRequest,
    InternalServerError
  } = require("../../helpers/responseHelper");
  const { getHashString, getRandomString, getPrivateKey } = require("../../helpers/hashHelper");
  
  const EXISTED_ACCOUNT = "This account existed";
  
  const register = async (req, res) => {
    const bodyData = getAccountFromBodyRequest(req);
    if (!bodyData || bodyData.role === "admin") return BadRequest(res, "invalid data");
    try {
      const account = await getAccountByUsername(bodyData.username);
      if (account) return BadRequest(res, EXISTED_ACCOUNT);
      const accountData = hashPasswordOfAccount(bodyData);
      const savingAccountResult = await insertAccount(accountData);
      const userInfoData = {
        username: savingAccountResult.username,
        displayName: bodyData.displayName || savingAccountResult.username
      };
      const savingUserInfoResult = await insertUserInfo(userInfoData);
      const result = getResponseObject(savingAccountResult, savingUserInfoResult);
      res.status(201).json(result);
    } catch (error) {
      InternalServerError(res);
      console.log(error);
    }
  };
  
  const getResponseObject = (account, userInfo) => {
    return {
      username: account.username,
      displayName: userInfo.displayName,
      role: account.role,
      join_date: account.createdDay
    };
  };
  
  const hashPasswordOfAccount = account => {
    const privateKey = getPrivateKey();
    const hashPassword = getHashString(account.password, privateKey);
    const accountData = {
      username: account.username,
      password: hashPassword,
      privateKey: privateKey,
      role: account.role,
      status: true
    };
    return accountData;
  };
  
  const getAccountFromBodyRequest = req => {
    if (!req.body) return null;
    let { username, password, displayName, role } = req.body;
    if (username && password) {
      username = username.trim();
      password = password.trim();
      role = role.trim();
      if (username == "" || password == ""|| role == "") {
        return null;
      }
      return { username, password, displayName, role };
    } else {
      return null;
    }
  };
  
  module.exports = register;
  