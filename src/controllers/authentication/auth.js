const jwt = require("jsonwebtoken");
const {
  InternalServerError,
  Unauthorized,
  Forbidden,
  Ok
} = require("../../helpers/responseHelper");
const appSettings = require("../../propertys");
const { getAccountById } = require("../../services/accountServices");
const LockedUser = "you are blocked, please contact admin for more detail!";

const auth = async (req, res) => {
  try {
    let token = req.headers["x-access-token"] || req.headers["authorization"];
    if (token && token.startsWith("JWT ")) {
      token = token.split(" ")[1];
    }
    if (token) {
      jwt.verify(token, appSettings.SECRETKEY, async (err, decoded) => {
        if (err) {
          return Unauthorized(res, "Invalid Token!");
        }
        const {status} = await getAccountById(decoded.id);
        if (!status) return Unauthorized(res, LockedUser);
        Ok(res, "you logined");
      });
    } else Forbidden(res, "Not found Token !");
  } catch (e) {
    console.log("REQUIRED LOGIN ERROR", e);
    InternalServerError(res);
  }
};

module.exports = auth;