const {
  changeRoleForAdmin,
  getAccountById
} = require("../../services/accountServices");
const {
  InternalServerError,
  BadRequest,
  Ok
} = require("../../helpers/responseHelper");

const changeRoleSystem = async (req, res) => {
  const { id } = req.params;
  const { role } = req.body;
  try {
    const account = await getAccountById(id);
    if (!account) {
      return BadRequest(res);
    }
    await changeRoleForAdmin(id, role);
    Ok(res, "role was changed");
  } catch (e) {
    console.log(e);
    InternalServerError(res);
  }
};

module.exports = changeRoleSystem;
