const {
    changePassword,
    getAccountById
  } = require("../../services/accountServices");
  const { getHashString } = require("../../helpers/hashHelper");
  const {
    InternalServerError,
    BadRequest,
    Ok
  } = require("../../helpers/responseHelper");
  
  const changePass = async (req, res) => {
    const { oldpass, newpass } = req.body;
    const { id } = req.decoded;
    if(!oldpass || !newpass) return BadRequest(res, "invalid data");
    try {
      const { password , privateKey} = await getAccountById(id);
      if (hash_password !== getHashString(oldpass, privateKey))
        return BadRequest(res, "Old password is not correct");
      await changePassword(id, getHashString(newpass, privateKey));
      Ok(res, "Password was changed");
    } catch (e) {
      console.log(e);
      InternalServerError(res);
    }
  };
  
  module.exports = changePass;