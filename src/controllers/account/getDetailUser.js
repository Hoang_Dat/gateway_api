const { getUserInfoByUsername } = require("../../services/userInforServices");
const {
  InternalServerError,
  BadRequest
} = require("../../helpers/responseHelper");

const getDetailUser = async (req, res) => {
  let { id } = req.params;
  try {
    const result = await getUserInfoByUsername(id);
    res.send(result);
  } catch (e) {
    console.log(e);
    InternalServerError(res);
  }
};

module.exports = getDetailUser;