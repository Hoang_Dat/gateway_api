const { updateUserInfoByUsername } = require("../../services/userInforServices");
const {
    InternalServerError,
    NotFound
  } = require("../../helpers/responseHelper");

const updateUserInfo = async (req, res) => {
  const { id: username } = req.params;
  const { email, 
    displayName, 
    address,
    gender,
    phoneNumber} = req.body;
    console.log("anhuhaudhua",req)
  try {
    const userInfor = {
      email : email,
      displayName : displayName,
      address : address,
      gender : gender,
      avatar : req.file.filename,
      phoneNumber : phoneNumber
    }
    const result = await updateUserInfoByUsername(username, userInfor);
    console.log("sdsdf",result)
    if (!result) return NotFound(res, username + " is not found");
    res.send(result);
  } catch (error) {
    if ((error.name === "CastError" || error.name === "SyntaxError")) return BadRequest(res, "Invalid data");
    InternalServerError(res);
  }
};

module.exports = updateUserInfo;