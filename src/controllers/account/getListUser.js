const { getAccountByStatusAndRole } = require("../../services/accountServices");
const {
  InternalServerError,
  BadRequest
} = require("../../helpers/responseHelper");
const { getUserInfoByUsername } = require("../../services/userInforServices");
const getListUser = async (req, res) => {
  let { status } = req.query;
  try {
    const account = await getAccountByStatusAndRole(status);
    const result = []
    await asyncForEach(account, async (ele) => {
        const data = await getUserInfoByUsername(ele.username);
        result.push(mapperElemetResponse(ele, data))
      });
    res.send(result);
  } catch (e) {
    console.log(e);
    InternalServerError(res);
  }
};
const asyncForEach = async (array, callback) =>{
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}
const mapperElemetResponse = (data , infor) => {
 return {
    id : data._id,
    username : data.username,
    status : data.status,
    role : data.role,
    displayName : infor.displayName,
    avatar : infor.avatar,
    email : infor.emain,
    gender : infor.gender,
    address : infor.address,
    createdDay : infor.createdDay,
 }
}
module.exports = getListUser;
