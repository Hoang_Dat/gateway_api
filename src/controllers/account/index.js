const adminRouter = require("express").Router();
const router = require("express").Router();
const mongoose = require('mongoose')
const upload = require('../../common/uploadImage')
const Grid = require('gridfs-stream');
var image_DB = require("../../config/image_DB");
const { requiredAdmin, requiredLogin } = require("../../middlewares/authentication");
let gfs;
image_DB.once('open', function () {
  console.log("image database read succes.");
  gfs = Grid(image_DB.db, mongoose.mongo);
  gfs.collection('upload');
}).on('error', function (error) {
  console.log("image database read false: " + error);
})
adminRouter.put("/", requiredAdmin, require("./blockAccount"));
adminRouter.get("/", requiredAdmin, require("./getListUser"));
adminRouter.patch("/", requiredAdmin, require("./changePasswordForAdmin"));
adminRouter.patch("/:id", requiredAdmin, require("./changeRole"));

router.patch("/", requiredLogin, require("./changePasswordAccount"));
router.get("/", requiredLogin, require("./getListCollaborator"));
router.get("/:id", requiredLogin, require("./getDetailUser"));
const singleUpload = upload.single("Image")
router.put("/:id", [requiredLogin, singleUpload], require("./updateUserInfor"));
module.exports = { adminRouter, router };