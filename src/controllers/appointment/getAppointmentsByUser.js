const {
    InternalServerError,
    BadRequest,
    Ok
  } = require("../../helpers/responseHelper");
const { getAccountByUsername } =require('../../services/accountServices');
const appointmentServices_GRPC = require("../../config/appointmentServices_GRPC");
const getAppointmentByUser = async (req, res) => {
    const { userId, status } = req.query;
    const account = await getAccountByUsername(userId);
    const appointmentRequest = {
        userId : userId,
        status   : status,
        role  : account.role
    }
    console.log(appointmentRequest)
    appointmentServices_GRPC.ListAppointmentByUser(appointmentRequest, (error, appointments) => {
     console.log(appointments)  
      if(!error) {
           res.send(appointments);
        }
        else {
          console.log(error);
          InternalServerError(res);
        }
    })
}
module.exports = getAppointmentByUser;