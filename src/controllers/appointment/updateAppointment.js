const {
    InternalServerError,
    BadRequest,
    Ok
  } = require("../../helpers/responseHelper");
const appointmentServices_GRPC = require("../../config/appointmentServices_GRPC");
const updateAppointment = async (req, res) => {
    console.log("fdff",req.body)
    const { appointment } = req.body;
    const appointmentRequestUpdate = {
        id : req.params.id,
        appointment : appointment
    };
    appointmentServices_GRPC.UpdateAppointment(appointmentRequestUpdate,(error, Appointment) => {
        if(!error) {
            Ok(res, "Modify appointment successfull")
        }
        else {
          console.log(error);
          InternalServerError(res);
        }
    })
}
module.exports = updateAppointment;