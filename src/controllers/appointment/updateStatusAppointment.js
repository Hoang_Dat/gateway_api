const {
    InternalServerError,
    BadRequest,
    Ok
  } = require("../../helpers/responseHelper");
const appointmentServices_GRPC = require("../../config/appointmentServices_GRPC");
const changeStatusAppointment = async (req, res) => {
    console.log(req.body)
    const { status } = req.body;
    const appointmentRequestStatus = {
        id : req.params.id,
        status : status
    };
    appointmentServices_GRPC.UpdateStatusAppointmnent(appointmentRequestStatus,(error, Appointment) => {
        if(!error) {
            Ok(res, "Change status appointment successfull")
        }
        else {
          console.log(error);
          InternalServerError(res);
        }
    })
}
module.exports = changeStatusAppointment;