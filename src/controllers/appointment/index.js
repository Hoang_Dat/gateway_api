const router = require("express").Router();
const { requiredAdmin, requiredLogin } = require("../../middlewares/authentication");

router.post("/", requiredLogin, require("./insertAppointment"));
router.get("/", requiredLogin, require("./getAppointmentsByUser"));
router.put("/:id", requiredLogin, require("./updateAppointment"));
router.patch("/:id", requiredLogin, require("./updateStatusAppointment"));

module.exports = { router };