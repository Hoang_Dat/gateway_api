const {
    InternalServerError,
    BadRequest,
    Ok
  } = require("../../helpers/responseHelper");
const appointmentServices_GRPC = require("../../config/appointmentServices_GRPC");
const insertAppointment = async (req, res) => {
    console.log(req.body)
    const { idPlace, 
        createBy, 
        hours,
        topic,
        collaborator,
        status,
        dateAppointement} = req.body;
    const appointmentRequest = {
        idPlace : idPlace,
        createBy : createBy,
        hours : hours,
        topic : topic,
        collaborator : collaborator,
        status : status,
        dateAppointement : dateAppointement
    };
    appointmentServices_GRPC.InsertAppointment(appointmentRequest,(error, Place) => {
        if(!error) {
           Ok(res,"Insert Successful");
        }
        else {
          console.log(error);
          InternalServerError(res);
        }
    })
}
module.exports = insertAppointment;