const {
    InternalServerError,
    BadRequest,
    Ok
} = require("../../helpers/responseHelper");
const EC = require('elliptic').ec;
const ec = new EC('secp256k1');
const transactionServices_GRPC = require("../../config/blockchainServices_GRPC");
const { getAccountById, getUserNameByPrivateKey } = require("../../services/accountServices");

const getTransactionForWallet = async (req, res) => {
    const { id } = req.params;
    const detailAccount = await getAccountById(id);
    transactionServices_GRPC.GetTransaction({ address: detailAccount.privateKey }, (error, transactions) => {
        if (!error) {
            res.send(transactions);
        }
        else {
            console.error(error);
            InternalServerError(res, "Get Transaction Faillure");
        }
    })
}
// const mapperTransactionForUser = (transactions) => {
//     console.log("cscsdvsvcvds",transactions)
//     return transactions.map(async ele => {
//         console.log("fromAddress", ele.fromAddress)
//         console.log("toAddress", ele.toAddress)
//         const clientKey = ec.keyFromPublic(ele.fromAddress,"hex");
//         const collaboratorKey = ec.keyFromPublic(ele.toAddress,"hex");
//         console.log("ele", clientKey.getPrivate())
//         return {
//             fromUSer:  getUserNameByPrivateKey(clientKey.getPrivate()).username,
//             toUSer:  getUserNameByPrivateKey(collaboratorKey.getPrivate()).username,
//             amount: ele.amount,
//             timestamp: ele.timestamp
//         }
//     })
// }
module.exports = getTransactionForWallet;