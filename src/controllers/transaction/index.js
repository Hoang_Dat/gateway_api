const router = require("express").Router();
const { requiredAdmin, requiredLogin } = require("../../middlewares/authentication");

router.post("/", requiredLogin, require("./insertTransaction"));
router.get("/:id", requiredLogin, require("./getTransactionForWallet"));

module.exports = { router };