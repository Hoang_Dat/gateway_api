const {
    InternalServerError,
    BadRequest,
    Ok
  } = require("../../helpers/responseHelper");
const transactionServices_GRPC = require("../../config/blockchainServices_GRPC");
const { getAccountById} = require("../../services/accountServices");
const insertTransactions = async (req, res) => {
    console.log(req.body)
    const { clientId, collaboratorId, amount} = req.body;
    const clientAccount = await getAccountById(clientId);
    const collaboratorAccount = await getAccountById(collaboratorId);
    console.log(clientAccount)
    console.log(collaboratorAccount)
    const transactionRequest = {
        fromAddress : clientAccount.privateKey,
        toAddress   : collaboratorAccount.privateKey,
        amount      : amount
    };
    transactionServices_GRPC.InsertTransaction(transactionRequest,(error, Transaction) => {
        if(!error) {
           Ok(res,"Insert Successful");
        }
        else {
          console.log(error);
          InternalServerError(res);
        }
    })
}
module.exports = insertTransactions;