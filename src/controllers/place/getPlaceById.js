const {
    InternalServerError,
    BadRequest,
    Ok
  } = require("../../helpers/responseHelper");
const informationServices_GRPC = require("../../config/informationServices_GRPC");
const getPlaceById = async (req, res) => {
    const { id } = req.params;
    informationServices_GRPC.GetPlaceById({ id : id },(error, place) => {
        if(!error) {
           res.send(place);
        }
        else {
          console.log(error);
          InternalServerError(res);
        }
    })
}
module.exports = getPlaceById;