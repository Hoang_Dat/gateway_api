const router = require("express").Router();
const mongoose = require('mongoose')
const upload = require('../../common/uploadImage')
const Grid = require('gridfs-stream');
var image_DB = require("../../config/image_DB");
const { requiredAdmin, requiredLogin } = require("../../middlewares/authentication");
let gfs;
image_DB.once('open', function () {
  console.log("image database read succes.");
  gfs = Grid(image_DB.db, mongoose.mongo);
  gfs.collection('upload');
}).on('error', function (error) {
  console.log("image database read false: " + error);
})
const singleUpload = upload.single("Image")
router.post("/", [requiredAdmin, singleUpload], require("./insertPlace"));
router.get("/", requiredLogin, require("./getPlaces"));
router.get("/:id", requiredLogin, require("./getPlaceById"));
router.delete("/:id", requiredAdmin, require("./deletePlace"));
router.put("/:id", requiredAdmin, require("./updatePlace"));

module.exports = { router };