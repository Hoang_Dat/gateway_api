const {
    InternalServerError,
    BadRequest,
    Ok
  } = require("../../helpers/responseHelper");
const informationServices_GRPC = require("../../config/informationServices_GRPC");
const insertPlace = async (req, res) => {
    console.log("dfefenfueu", req.body)
    console.log("xksjsihis", req.file)
    const {  lat, 
            long,
            province_city,
            district,
            street,
            title,
            content,
            likes} = req.body;
    const placeRequest = {
        lat : lat,
        long : long,
        province_city : province_city,
        district : district,
        street : street,
        title : title,
        content : content,
        imageView : req.file.filename,
        likes : likes
    };
    informationServices_GRPC.InsertPlace(placeRequest, (error, Place) => {
        if(!error) {
           Ok(res,"Insert Successful");
        }
        else {
          console.log(error);
          InternalServerError(res);
        }
    })
}
module.exports = insertPlace;