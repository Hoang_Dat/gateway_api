const {
    InternalServerError,
    BadRequest,
    Ok
  } = require("../../helpers/responseHelper");
const informationServices_GRPC = require("../../config/informationServices_GRPC");
const deletePlace = async (req, res) => {
    const { id } = req.params;
    informationServices_GRPC.DeletePlace({ id : id },(error, Transaction) => {
        if(!error) {
           Ok(res,"Delete Successful");
        }
        else {
          console.log(error);
          InternalServerError(res);
        }
    })
}
module.exports = deletePlace;