const router = require("express").Router();
const upload = require('../../common/uploadImage')
var mongoose = require('mongoose')
var Grid = require('gridfs-stream');
var image_DB = require("../../config/image_DB")
const { requiredAdmin, requiredLogin } = require("../../middlewares/authentication");

let gfs;
image_DB.once('open', function () {
  console.log("image database read succes.");
  gfs = Grid(image_DB.db, mongoose.mongo);
  gfs.collection('upload');
}).on('error', function (error) {
  console.log("image database read false: " + error);
})
const singleUpload = upload.single("Image")
router.post('/', [singleUpload, requiredLogin] , (req, res) => {
  return res.status(200).json({ result: 'success', file: req.file })
})
router.get('/', requiredLogin, (req, res) => {
  gfs.files.find().toArray((err, files) => {
    if (!files || files.length === 0) {
      return res.status(404).json({
        result: 'No image exist'
      });
    }
    return res.status(200).json({ result: 'success', data: files });
  });
})
router.get('/:filename', (req, res) => {
  gfs.files.findOne({ filename: req.params.filename }, (err, file) => {
    console.log(file)
    if (!file || file.length === 0) {
      return res.status(404).json({
        err: 'No image exists'
      });
    }
    if (file.contentType === 'image/jpeg' || file.contentType === 'image/png') {
      const readstream = gfs.createReadStream(file.filename);
      readstream.pipe(res);
    } else {
      res.status(404).json({
        err: 'Not an image'
      });
    }
  });
});
router.delete('/:id',requiredLogin,  (req, res) => {
  gfs.remove({ _id: req.params.id, root: 'upload' }, (err, gridStore) => {
    if (err) {
      return res.status(404).json({ err: err });
    }
    res.status(200).json({ result: "success" });
  });
});
module.exports = { router }