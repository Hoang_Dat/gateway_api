const {
    InternalServerError,
    BadRequest,
    Ok
  } = require("../../helpers/responseHelper");
const informationServices_GRPC = require("../../config/informationServices_GRPC");
const insertTopics = async (req, res) => {
    console.log(req.body)
    const { name, description} = req.body;
    const topicRequest = {
        name : name,
        description : description
    };
    informationServices_GRPC.InsertTopic(topicRequest,(error, Transaction) => {
        if(!error) {
           Ok(res,"Insert Successful");
        }
        else {
          console.log(error);
          InternalServerError(res);
        }
    })
}
module.exports = insertTopics;