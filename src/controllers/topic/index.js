const router = require("express").Router();
const { requiredAdmin, requiredLogin } = require("../../middlewares/authentication");

router.post("/", requiredAdmin, require("./insertTopic"));
router.get("/", requiredLogin, require("./getTopics"));
router.delete("/:id", requiredAdmin, require("./getTopics"));

module.exports = { router };