const {
    InternalServerError,
    BadRequest,
    Ok
  } = require("../../helpers/responseHelper");
const chattingServices_GRPC = require("../../config/chattingServices_GRPC");
const addRoomChat = async (req, res) => {
    console.log(req.body)
    const { clientUser, collaboratorUser} = req.body;
    const chatRoomRequest = {
        clientUser : clientUser,
        collaboratorUser : collaboratorUser,
    };
    chattingServices_GRPC.InsertRoomChat(chatRoomRequest,(error, RoomChat) => {
        if(!error) {
           Ok(res,"Insert Successful");
        }
        else {
          console.log(error);
          InternalServerError(res);
        }
    })
}
module.exports = addRoomChat;