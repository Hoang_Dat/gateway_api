const {InternalServerError,
    BadRequest,
    Ok
} = require("../../helpers/responseHelper");
const { getAccountByUsername } = require('../../services/accountServices');
const chattingServices_GRPC = require("../../config/chattingServices_GRPC");
const getRoomChatDetail = async (req, res) => {
    const { id } = req.params;
    chattingServices_GRPC.GetRoomChatDetail({id : id}, (error, chatMessages) => {
        console.log(chatMessages)
        if (!error) {
            res.send(chatMessages);
        }
        else {
            console.log(error);
            InternalServerError(res);
        }
    })
}
module.exports = getRoomChatDetail;