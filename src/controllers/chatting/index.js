const router = require("express").Router();
const { requiredAdmin, requiredLogin } = require("../../middlewares/authentication");

router.post("/", requiredLogin, require("./addRoomChat"));
router.post("/message", requiredLogin, require("./addmessage"));
router.get("/", requiredLogin, require("./getAllConversations"));
router.get("/:id", requiredLogin, require("./getRoomChatDetail"));

module.exports = { router };