const {
    InternalServerError,
    BadRequest,
    Ok
  } = require("../../helpers/responseHelper");
const { getAccountByUsername } = require('../../services/accountServices');
const { getUserInfoByUsername } = require('../../services/userInforServices');
const chattingServices_GRPC = require("../../config/chattingServices_GRPC");
const getAllRoomChatByUser = async (req, res) => {
    const { userId } = req.query;
    console.log("getallroom", userId)
    const account = await getAccountByUsername(userId);
    const roomChatRequest = {
        userId : userId,
        role  : account.role
    }
    chattingServices_GRPC.ListRoomChatByUser(roomChatRequest, async (error, roomChattings) => {
      if(!error) {
          const data = await  mapperModelResponse(roomChattings.roomChattings);
          console.log("datda", data)
           res.send(data);
        }
        else {
          console.log(error);
          InternalServerError(res);
        }
    })
}

const mapperModelResponse =   (rooms) => {
    const promises = rooms.map(async (element) => {
       const clientInfor = await getUserInfoByUsername(element.clientUser);
       const collaboratorInfor = await getUserInfoByUsername(element.collaboratorUser)
       return {
           id : element.id,
           clientUser : clientInfor,
           collaboratorUser : collaboratorInfor,
           createdDate : element.createdDate
       }
    })
    return Promise.all(promises);
}
module.exports = getAllRoomChatByUser;