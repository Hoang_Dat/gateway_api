const {
    InternalServerError,
    BadRequest,
    Ok
  } = require("../../helpers/responseHelper");
const chattingServices_GRPC = require("../../config/chattingServices_GRPC");
const addMessageChat = async (req, res) => {
    console.log(req.body)
    const { roomId, message, createdBy} = req.body;
    const chatMesageRequest = {
        roomId : roomId,
        message : message,
        createdBy : createdBy,
    };
    console.log(chatMesageRequest)
    chattingServices_GRPC.InsertMessageChat(chatMesageRequest,(error, ChatMessage) => {
        if(!error) {
           Ok(res,"Insert Successful");
        }
        else {
          console.log(error);
          InternalServerError(res);
        }
    })
}
module.exports = addMessageChat;