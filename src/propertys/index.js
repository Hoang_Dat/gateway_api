const dotenv = require('dotenv');
dotenv.config()

module.exports = {
  PORT: process.env.PORT,
  MONGODB_URL: process.env.MONGODB_URl,
  IMAGE_MONGODB_URL: process.env.IMAGE_MONGODB_URL,
  BLOCKCHAIN_SERVICE_HOST: process.env.BLOCKCHAIN_SERVICE_HOST,
  SECRETKEY : process.env.SECRETKEY,
  UPLOAD_COLLECTION : process.env.UPLOAD_COLLECTION,
  TOKEN_EXPIRE_TIMELONG : "6 days",
  TOKEN_EXPIRE_TIMESHORT : "2 days"
};