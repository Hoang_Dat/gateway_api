const Account = require("../models/account");
const { ACCOUNT_STATUS } = require("../helpers/constants");

const insertAccount = async account => {
  const acc = new Account(account);
  const result = await acc.save();
  return result;
};

const getAccountByUsername = async username => {
  console.log(username)
  const a =await Account.findOne({ username });;
  console.log(a)
  return await Account.findOne({ username });
};

const getAccountById = async id => {
  return await Account.findById(id);
};

const getUserRoleById = async id => {
  return await Account.findById(id).select("role");
};

const getUserNameByPrivateKey = privateKey => {
  console.log("privateKey",privateKey)
  return  Account.findOne({privateKey : privateKey});
};

const changePassword = async (id, newPass) => {
  return await Account.findByIdAndUpdate(id, { password: newPass });
};

const ManageAccountById = async (id, status) => {
  return await Account.findByIdAndUpdate(id, { status }, { new: true });
};

const getTotalPage = async (pageSize, query) => {
  const count = await Account.countDocuments(query);
  return Math.ceil(count / pageSize);
};

const filterByRole = async (query, role) => {
  if(role != "all")
        query = await query
    .where('role').equals(role)
  return query;
};

const getAccountByStatusAndRole = async (status, role = "all") => {
  let query;
  switch (status) {
    case ACCOUNT_STATUS.ACTIVE:
      query = Account.find({ status: ACCOUNT_STATUS.ACTIVE });
      break;
    case ACCOUNT_STATUS.BLOCKED:
      query = Account.find({ status: ACCOUNT_STATUS.BLOCKED });
      break;
    default:
      query = Account.find();
  }

  return await filterByRole(query, role);
};

const changeRoleForAdmin = async (id, newRole) => {
    return Account.findByIdAndUpdate(id, { role: newRole });
};

const countTotalAccount =async ()=>{
  return await Account.countDocuments();
}

module.exports = {
  insertAccount,
  getAccountByUsername,
  getUserRoleById,
  ManageAccountById,
  getAccountById,
  getAccountByStatusAndRole,
  changePassword,
  changeRoleForAdmin,
  countTotalAccount,
  getUserNameByPrivateKey
};
