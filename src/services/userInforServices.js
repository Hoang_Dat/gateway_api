const UserInfo = require("../models/user");

const insertUserInfo = async userInfoData => {
  const userInfo = new UserInfo(userInfoData);
  return await userInfo.save();
}
const getUserInfoByUsername = async username => {
  console.log(username)
  return await UserInfo.findOne({ username: username });
};
const updateUserInfoByUsername = async (username, userInfoData) => {
  return await UserInfo.findOneAndUpdate({ username: username }, userInfoData, {
    new: true
  });
};
module.exports = {
    insertUserInfo,
    getUserInfoByUsername,
    updateUserInfoByUsername
  };